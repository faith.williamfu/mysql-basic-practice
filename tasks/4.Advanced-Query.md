# 高级查询
## Tasks：
基于前面一节创建的数据库，请完成以下Tasks：
##### 1. 查询语文期末考试的所有成绩。
##### 2. 查询参加语文中期考试的所有学生。
##### 3. 查询参加语文期末考试及格的学生(分数大于等于60)。
##### 4. 查询参加李老师所带科目期末考试及格的学生(分数大于等于60)。
##### 5. 查询参加李老师所带科目期末考试的平均成绩。
##### 6. 查询参加李老师所带科目所有考试的最高分。
##### 7. 查询参加李老师所带科目所有考试的最低分。
##### 8. 查询张三同学期末考试所有科目的总分。
##### 9. 更新张三同学期末考试语文的分数为88分。
##### 10. 查询参加中期考试的学生人数。

## Your Answers：
请将每个task的实现SQL和查询结果写在下面。
##### 1. 查询语文期末考试的所有成绩。 88, 40, 48, 78
SELECT 
    score
FROM
    score
WHERE
    exam_id = (SELECT 
            id
        FROM
            exam
        WHERE
            `name` = '期末考试'
                AND subject_id = (SELECT 
                    id
                FROM
                    subjects
                WHERE
                    `name` = '语文'));
##### 2. 查询参加语文中期考试的所有学生。 张三. 李四, 王五
SELECT 
    `name`
FROM
    student
WHERE
    id IN (SELECT 
            student_id
        FROM
            score
        WHERE
            exam_id = (SELECT 
                    id
                FROM
                    exam
                WHERE
                    `name` = '中期考试'
                        AND subject_id = (SELECT 
                            id
                        FROM
                            subjects
                        WHERE
                            `name` = '语文')));
##### 3. 查询参加语文期末考试及格的学生(分数大于等于60)。 张三, 王五
SELECT 
    `name`
FROM
    student
WHERE
    id IN (SELECT 
            student_id
        FROM
            score
        WHERE
            exam_id = (SELECT 
                    id
                FROM
                    exam
                WHERE
                    `name` = '中期考试'
                        AND subject_id = (SELECT 
                            id
                        FROM
                            subjects
                        WHERE
                            `name` = '语文'))
                AND score >= 60);
##### 4. 查询参加李老师所带科目期末考试及格的学生(分数大于等于60)。 张三, 赵六
SELECT 
    `name`
FROM
    student
WHERE
    id IN (SELECT 
            student_id
        FROM
            score
        WHERE
            exam_id = (SELECT 
                    id
                FROM
                    exam
                WHERE
                    `name` = '期末考试'
                        AND subject_id = (SELECT 
                            subject_id
                        FROM
                            teacher
                        WHERE
                            `name` = '李老师'))
                AND score >= 60);
##### 5. 查询参加李老师所带科目期末考试的平均成绩。 54.25
SELECT 
    AVG(score)
FROM
    score
WHERE
    exam_id = (SELECT 
            id
        FROM
            exam
        WHERE
            `name` = '期末考试'
                AND subject_id = (SELECT 
                    subject_id
                FROM
                    teacher
                WHERE
                    `name` = '李老师'));
##### 6. 查询参加李老师所带科目所有考试的最高分。 88
SELECT 
    MAX(score)
FROM
    score
WHERE
    exam_id = (SELECT 
            id
        FROM
            exam
        WHERE
            `name` = '期末考试'
                AND subject_id = (SELECT 
                    subject_id
                FROM
                    teacher
                WHERE
                    `name` = '李老师'));
##### 7. 查询参加李老师所带科目所有考试的最低分。 30
SELECT 
    MIN(score)
FROM
    score
WHERE
    exam_id = (SELECT 
            id
        FROM
            exam
        WHERE
            `name` = '期末考试'
                AND subject_id = (SELECT 
                    subject_id
                FROM
                    teacher
                WHERE
                    `name` = '李老师'));
##### 8. 查询张三同学期末考试所有科目的总分。 148
SELECT 
    SUM(score)
FROM
    score
WHERE
    exam_id IN (SELECT 
            id
        FROM
            exam
        WHERE
            name = '期末考试')
        AND student_id = (SELECT 
            id
        FROM
            student
        WHERE
            name = '张三');
##### 9. 更新张三同学期末考试语文的分数为88分。
UPDATE score
SET
    score = 88
WHERE
    student_id = (SELECT 
            id
        FROM
            student
        WHERE
            name = '张三')
        AND exam_id = (SELECT 
            id
        FROM
            exam
        WHERE
            name = '期末考试'
                AND subject_id = (SELECT 
                    id
                FROM
                    subjects
                WHERE
                    name = '语文'));
##### 10. 查询参加中期考试的学生人数。 4
SELECT 
    COUNT(DISTINCT (student_id))
FROM
    score
WHERE
    exam_id IN (SELECT 
            id
        FROM
            exam
        WHERE
            name = '中期考试');

